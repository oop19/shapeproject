/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeprojact;

/**
 *
 * @author a
 */
public class Rectangle {
    private double w,l;
    public Rectangle(double w,double l){
        this.w = w;
        this.l = l;
    }
    public double calArea(){
        return w * l;
    }
    public double getL(){
        return l;
    }
    public double getW(){
        return w;
    }
    public void setLW(double l, double w){
        if (l  <= 0 || w <= 0){
            System.out.println("Error: length and width must more than zero!!!!");
            return;
        }
        this.l = l;
        this.w = w;
    }
    
}
