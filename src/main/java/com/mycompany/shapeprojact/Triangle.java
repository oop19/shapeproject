/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeprojact;

/**
 *
 * @author a
 */
public class Triangle {
   private double h,bl;
   public double f = 1.0/2;
   public Triangle(double h, double bl){
       this.h = h;
       this.bl = bl;
   }
   public double calArea(){
       return f * h * bl;
   }
   public double getH(){
       return h;
   }
   public double getBL(){
       return bl;
   }
   public void setHBL(double h, double bl){
       if (h <= 0 || bl <= 0){
           System.out.println("Error: Triangle must more than zero!!!");
           return;
       }
       this.h = h;
       this.bl = bl;
   }
   
}
